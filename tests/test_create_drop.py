from typing import Iterable
from sqletic import Engine

def test_create_table():
    statement = """
    create table cities (
                            name TEXT,
                            country TEXT,
                            planet TEXT)
    """

    database = {}
    engine = Engine(database)
    
    engine.execute(statement)
    assert 'cities' in database
    
def test_drop_table():
    statement = """drop table planets"""

    database = {"cities":[],
                "planets":[]}
    engine = Engine(database)
    
    engine.execute(statement)
    assert 'cities' in database
    assert 'planets' not in database


def main():
    test_create_table()
    test_drop_table()
